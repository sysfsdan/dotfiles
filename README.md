<div align="center">
  
### Arch Linux [KDE Plasma Catppuccin themed + Kitty + Zsh + Neofetch]

![Desktop](screenshot/desktop.png)

![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)
</div>


**This setup is all about simple customization, clean look, full functionality, it's designed by me for me.
I am using this set up for so long, but I am open for changes but I am not comfy doing them.**

**Shell information:**

| Package                                                  | Description                                                      |
|----------------------------------------------------------|------------------------------------------------------------------|
| [Arch Linux](https://www.archlinux.org/)                 | The best distro ever!                                            |
| [Catppuccin](https://github.com/catppuccin/catppuccin)   |  Soothing pastel theme for the high-spirited!                    |  
| [Zsh](https://github.com/zsh-users/zsh)                  | The most powerful shell out there!                               |
| [Oh-my-zsh](https://ohmyz.sh/)                           | The most awesome ZSH configuration framework                     |
| [neofetch](https://github.com/dylanaraps/neofetch)       | A fast, highly customizable system info script                   |

* Fonts used: Work Sans and Space Mono NF
* Icons used: Icons: Papirus [Lavender Catppuccin folders]
