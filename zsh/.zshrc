# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Keychain
eval $(keychain --eval daniel_ed25519)
#eval $(keychain --eval dnikoloski_github)

ZSH_THEME="half-life"

plugins=(git zsh-completions zsh-autosuggestions colored-man-pages kubectl)

source $ZSH/oh-my-zsh.sh

# Source alisases
source /home/sysfsdan/.config/shell/aliasrc

# History
HISTFILE="$HOME/.zsh_history"
HISTSIZE=10000
SAVEHIST=10000

export PATH=$PATH:/home/sysfsdan/.spicetify
